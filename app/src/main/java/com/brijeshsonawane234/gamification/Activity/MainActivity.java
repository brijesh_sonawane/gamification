package com.brijeshsonawane234.gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brijeshsonawane234.gamification.R;
import com.brijeshsonawane234.gamification.ViewModel.DataViewModel;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    TextView tv_header,tv_contestName,tv_contestNameData,tv_rs,tv_terms,tv_how1,tv_how2,tv_how3;
    Spinner spin;
    ImageView imageViewHead;
    String[] terms = { "Terms and Conditions" };
    DataViewModel dataViewModel=new DataViewModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataViewModel.getData();
        toolbar= (Toolbar)findViewById(R.id.mytoolbar);
        tv_header = (TextView) findViewById(R.id.tv1);
        tv_contestName = (TextView) findViewById(R.id.tv2);
        tv_contestNameData = (TextView) findViewById(R.id.tv3);
        tv_rs = (TextView) findViewById(R.id.tv4);
        tv_terms = (TextView) findViewById(R.id.tv5);
        tv_how1 = (TextView) findViewById(R.id.tv7);
        tv_how2 = (TextView) findViewById(R.id.tv8);
        tv_how3 = (TextView) findViewById(R.id.tv9);
        spin = (Spinner) findViewById(R.id.sp1);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item_1, terms);
        adapter.setDropDownViewResource(R.layout.spinner_item_1);
        spin.setAdapter(adapter);
        setSupportActionBar(toolbar);
    }

    public void fun(View view)
    {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
    }
}
