package com.brijeshsonawane234.gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brijeshsonawane234.gamification.R;

public class Main3Activity extends AppCompatActivity {

    private Toolbar toolbar;
    TextView tv_header,tv_data,tv_char1,tv_char2,tv_char3,tv_char4,tv_char5,tv_char6;
    Button bt_char1,bt_char2,bt_char3,bt_char4,bt_char5,bt_char6,bt_char7,bt_char8,bt_char9,bt_char10;
    ImageView imageViewHead,imageView;
    int count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        toolbar= (Toolbar)findViewById(R.id.mytoolbar);
        tv_header = (TextView) findViewById(R.id.tv1);
        tv_data = (TextView) findViewById(R.id.tv3);
        tv_char1 = (TextView) findViewById(R.id.tv4);
        tv_char2 = (TextView) findViewById(R.id.tv5);
        tv_char3 = (TextView) findViewById(R.id.tv6);
        tv_char4 = (TextView) findViewById(R.id.tv7);
        tv_char5 = (TextView) findViewById(R.id.tv8);
        tv_char6 = (TextView) findViewById(R.id.tv9);
        bt_char1 = (Button) findViewById(R.id.b1);
        bt_char2 = (Button) findViewById(R.id.b2);
        bt_char3 = (Button) findViewById(R.id.b3);
        bt_char4 = (Button) findViewById(R.id.b4);
        bt_char5 = (Button) findViewById(R.id.b5);
        bt_char6 = (Button) findViewById(R.id.b6);
        bt_char7 = (Button) findViewById(R.id.b7);
        bt_char8 = (Button) findViewById(R.id.b8);
        bt_char9 = (Button) findViewById(R.id.b9);
        bt_char10 = (Button) findViewById(R.id.b10);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);
        imageView = (ImageView) findViewById(R.id.imageView2);
        bt_char1.setText("A");
        bt_char2.setText("B");
        bt_char3.setText("C");
        bt_char4.setText("D");
        bt_char5.setText("E");
        bt_char6.setText("F");
        bt_char7.setText("G");
        bt_char8.setText("H");
        bt_char9.setText("I");
        bt_char10.setText("J");
        setSupportActionBar(toolbar);
    }

    //Text view (ans) click
    public void tvChar1(View view)
    {
        count=0;
    }
    public void tvChar2(View view)
    {
        count=1;
    }
    public void tvChar3(View view)
    {
        count=2;
    }
    public void tvChar4(View view)
    {
        count=3;
    }
    public void tvChar5(View view)
    {
        count=4;
    }
    public void tvChar6(View view)
    {
        count=5;
    }

    //button click
    public void btChar1(View view)
    {
        setText((String) bt_char1.getText());
        bt_char1.setEnabled(false);
    }
    public void btChar2(View view)
    {
        setText((String) bt_char2.getText());
        bt_char2.setEnabled(false);
    }
    public void btChar3(View view)
    {
        setText((String) bt_char3.getText());
        bt_char3.setEnabled(false);
    }
    public void btChar4(View view)
    {
        setText((String) bt_char4.getText());
        bt_char4.setEnabled(false);
    }
    public void btChar5(View view)
    {
        setText((String) bt_char5.getText());
        bt_char5.setEnabled(false);
    }
    public void btChar6(View view)
    {
        setText((String) bt_char6.getText());
        bt_char6.setEnabled(false);
    }
    public void btChar7(View view)
    {
        setText((String) bt_char7.getText());
        bt_char7.setEnabled(false);
    }
    public void btChar8(View view)
    {
        setText((String) bt_char8.getText());
        bt_char8.setEnabled(false);
    }
    public void btChar9(View view)
    {
        setText((String) bt_char9.getText());
        bt_char9.setEnabled(false);
    }
    public void btChar10(View view)
    {
        setText((String) bt_char10.getText());
        bt_char10.setEnabled(false);
    }


    //set textView (ans) using button
    public void setText(String data)
    {
        String str="";
        switch (count)
        {
            case 0:
                    if(tv_char1.getText().equals(""))
                    {
                        tv_char1.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char1.getText();
                        tv_char1.setText(data);
                        break;
                    }
            case 1:
                    if(tv_char2.getText().equals(""))
                    {
                        tv_char2.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char2.getText();
                        tv_char2.setText(data);
                        break;
                    }

            case 2:
                    if(tv_char3.getText().equals(""))
                    {
                        tv_char3.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char3.getText();
                        tv_char3.setText(data);
                        break;
                    }
            case 3:
                    if(tv_char4.getText().equals(""))
                    {
                        tv_char4.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char4.getText();
                        tv_char4.setText(data);
                        break;
                    }
            case 4:
                    if(tv_char5.getText().equals(""))
                    {
                        tv_char5.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char5.getText();
                        tv_char5.setText(data);
                        break;
                    }
            case 5:
                    if(tv_char6.getText().equals(""))
                    {
                        tv_char6.setText(data);
                        break;
                    }
                    else
                    {
                        str= (String) tv_char6.getText();
                        tv_char6.setText(data);
                        break;
                    }
        }
        if (!str.equals(""))
        {
            if (bt_char1.getText().equals(str))
            {
                bt_char1.setEnabled(true);
            }
            else
            {
                if (bt_char2.getText().equals(str))
                {
                    bt_char2.setEnabled(true);
                }
                else
                {
                    if (bt_char3.getText().equals(str))
                    {
                        bt_char3.setEnabled(true);
                    }
                    else
                    {
                        if (bt_char4.getText().equals(str))
                        {
                            bt_char4.setEnabled(true);
                        }
                        else
                        {
                            if (bt_char5.getText().equals(str))
                            {
                                bt_char5.setEnabled(true);
                            }
                            else
                            {
                                if (bt_char6.getText().equals(str))
                                {
                                    bt_char6.setEnabled(true);
                                }
                                else
                                {
                                    if (bt_char7.getText().equals(str))
                                    {
                                        bt_char7.setEnabled(true);
                                    }
                                    else
                                    {
                                        if (bt_char8.getText().equals(str))
                                        {
                                            bt_char8.setEnabled(true);
                                        }
                                        else
                                        {
                                            if (bt_char9.getText().equals(str))
                                            {
                                                bt_char9.setEnabled(true);
                                            }
                                            else
                                            {
                                                if (bt_char10.getText().equals(str))
                                                {
                                                    bt_char10.setEnabled(true);
                                                }
                                                else
                                                {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        count++;
    }




}
