package com.brijeshsonawane234.gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brijeshsonawane234.gamification.R;

public class Main2Activity extends AppCompatActivity {

    private Toolbar toolbar;
    TextView tv_header,tv_question,tv_opetion1,tv_opetion2,tv_opetion3,tv_opetion4;
    ImageView imageViewHead;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        toolbar= (Toolbar)findViewById(R.id.mytoolbar);
        tv_header = (TextView) findViewById(R.id.tv1);
        tv_question = (TextView) findViewById(R.id.tv3);
        tv_opetion1 = (TextView) findViewById(R.id.tv4);
        tv_opetion2 = (TextView) findViewById(R.id.tv5);
        tv_opetion3 = (TextView) findViewById(R.id.tv6);
        tv_opetion4 = (TextView) findViewById(R.id.tv7);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);
        tv_opetion1.setText("Option 1");
        tv_opetion2.setText("Option 2");
        tv_opetion3.setText("Option 3");
        tv_opetion4.setText("Option 4");
        setSupportActionBar(toolbar);
    }
    public void option1(View view)
    {

    }
    public void option2(View view)
    {

    }
    public void option3(View view)
    {

    }
    public void option4(View view)
    {
        Intent intent = new Intent(this, Main3Activity.class);
        startActivity(intent);
    }
}
