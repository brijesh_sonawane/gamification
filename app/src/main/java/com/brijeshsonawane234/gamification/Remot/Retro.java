package com.brijeshsonawane234.gamification.Remot;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retro
{
    private static final String rootUrl="";
    private static Retrofit getRetrofitInstance()
    {

        return new Retrofit.Builder().baseUrl(rootUrl).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static ApiRetrofit getApiRetrofit()
    {

        return getRetrofitInstance().create(ApiRetrofit.class);
    }
    public void prepareData()
    {
        ApiRetrofit apiRetrofit = Retro.getApiRetrofit();

        //Api call
    }
}
